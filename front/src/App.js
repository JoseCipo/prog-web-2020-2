import React from 'react';
import logo from './logo.svg';
import './App.css';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function App() {
  return (
    <div>
      {/*topo-------------------------------------------------------------------------------------*/}
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Navbar</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#features">Features</Nav.Link>
          <Nav.Link href="#pricing">Pricing</Nav.Link>
        </Nav>
      </Navbar>

      {/*conteudo-------------------------------------------------------------------------------------*/}
      <div>
        <Container fluid>
          <Row>
            {/*vertical*/}
            <Col lg={2}>
              <Nav defaultActiveKey="/home" className="flex-column">
                <Nav.Link href="/home">Active</Nav.Link>
                  <Nav.Link eventKey="link-1">Link</Nav.Link>
                  <Nav.Link eventKey="link-2">Link</Nav.Link>
                  <Nav.Link eventKey="disabled" disabled>
                    Disabled
                </Nav.Link>
              </Nav>
            </Col>

          {/*dinamico*/}
            <Col lg={10}>
              <Row>
                <Col>
                  <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src="img/fusca.jpg" />
                    <Card.Body>
                      <Card.Title>Fusca</Card.Title>
                        <Card.Text>
                          Some quick example text to build on the card title and make up the bulk of
                          the card's content.
                        </Card.Text>
                        <Button variant="primary">Go somewhere</Button>
                    </Card.Body>
                  </Card>
                </Col>

                <Col>
                  <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src="img/fusca.jpg" />
                    <Card.Body>
                      <Card.Title>Fusca</Card.Title>
                        <Card.Text>
                          Some quick example text to build on the card title and make up the bulk of
                          the card's content.
                        </Card.Text>
                        <Button variant="primary">Go somewhere</Button>
                    </Card.Body>
                  </Card>
                </Col>

                <Col>
                  <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src="img/fusca.jpg" />
                    <Card.Body>
                      <Card.Title>Fusca</Card.Title>
                        <Card.Text>
                          Some quick example text to build on the card title and make up the bulk of
                          the card's content.
                        </Card.Text>
                        <Button variant="primary">Go somewhere</Button>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>

            </Col>
          </Row>
        </Container>
      </div>

      {/*rodapé-------------------------------------------------------------------------------------*/}

      <Container fluid style={{backgroundColor:"#ADD8E6"}}>
        <Row>
          <Col md={2}>
            <Nav defaultActiveKey="/home" className="flex-column">
              <Nav.Link href="/home">Active</Nav.Link>
                  <Nav.Link eventKey="link-1">Link</Nav.Link>
                  <Nav.Link eventKey="link-2">Link</Nav.Link>
                  <Nav.Link eventKey="disabled" disabled>
                  Disabled
              </Nav.Link>
            </Nav>
          </Col>

          <Col md={{ span: 3, offset: 7 }}>
            <h2>Contato</h2>
            <p>store@garagem.com</p>
            <p>(62) 9999-9999</p>
            <p>Rua Raio, Centro, 62. Goiânia, Goiás, Brasil.</p>
          </Col>
        </Row>
      </Container>

    </div>
  );
}

export default App;
